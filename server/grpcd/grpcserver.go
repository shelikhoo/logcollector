package grpcd

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	log "github.com/sirupsen/logrus"
	"github.com/xiaokangwang/torprobeutil/common/def"
	"github.com/xiaokangwang/torprobeutil/server/summary"
	"google.golang.org/grpc"
	"os"
)

/*
var keyFile = func() *os.File {
	fd, err := os.OpenFile("debug_keyout", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	return fd
}()
*/

func StartServer() error {
	cert, err := tls.LoadX509KeyPair("server.pem", "server.key")
	if err != nil {
		log.Fatalf("load key: %s", err)
	}

	certPEMBlock, err := os.ReadFile("ca.pem")
	if err != nil {
		log.Fatalf("load ca: %s", err)
	}
	cacertByte, _ := pem.Decode(certPEMBlock)
	cacert, err := x509.ParseCertificate(cacertByte.Bytes)
	if err != nil {
		log.Fatalf("load ca: %s", err)
	}
	pool := x509.NewCertPool()
	pool.AddCert(cacert)
	config := &tls.Config{Certificates: []tls.Certificate{cert}, ClientAuth: tls.RequireAndVerifyClientCert, ClientCAs: pool, NextProtos: []string{"h2"}}
	list, err := tls.Listen("tcp", mustGetConfFromEnv("LISTEN_ADDR"), config)
	if err != nil {
		log.Fatalf("tls listen: %s", err)
	}
	var opts []grpc.ServerOption
	logger := log.WithField("module", "gRPC")
	opts = append(opts, grpc_middleware.WithUnaryServerChain(
		grpc_logrus.UnaryServerInterceptor(logger),
	),
		grpc_middleware.WithStreamServerChain(
			grpc_logrus.StreamServerInterceptor(logger),
		))
	grpcServer := grpc.NewServer(opts...)
	def.RegisterProbeTelemetryServiceServer(grpcServer, summary.NewProbeTelemetryService())
	log.Println(grpcServer.Serve(list).Error())
	return nil
}

func mustGetConfFromEnv(name string) string {
	data, ok := os.LookupEnv(name)
	if ok {
		return data
	}
	panic(fmt.Sprintf("no env %v", name))
}
