package jsonllog

import (
	"context"
	"fmt"
	"github.com/xiaokangwang/torprobeutil/common/def"
	"github.com/xiaokangwang/torprobeutil/server/summary"
	"google.golang.org/protobuf/encoding/protojson"
	"os"
)

func init() {
	summary.RegisterResultCollector("jsonl", NewJsonLogger("jsonlog.jsonl"))
}

func NewJsonLogger(filepath string) summary.ProbeResultCollector {
	fd, err := os.OpenFile(filepath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	return &jsonLogger{output: fd}
}

type jsonLogger struct {
	output *os.File
}

func (j *jsonLogger) SubmitProbeResult(ctx context.Context, req *def.SubmitProbeResultReq) error {
	for _, value := range req.Summary {
		data, err := protojson.Marshal(value)
		if err != nil {
			continue
		}
		fmt.Fprintln(j.output, string(data))
	}
	return nil
}
