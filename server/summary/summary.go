package summary

import (
	"context"
	"fmt"
	"github.com/xiaokangwang/torprobeutil/common/def"
	"os"
	time2 "time"
)

type ProbeResultCollector interface {
	SubmitProbeResult(ctx context.Context, req *def.SubmitProbeResultReq) error
}

var probeResultCollectors = func() map[string]ProbeResultCollector {
	return map[string]ProbeResultCollector{}
}()

func RegisterResultCollector(name string, collector ProbeResultCollector) {
	probeResultCollectors[name] = collector
}

type probeTelemetryServiceServer struct {
	def.UnimplementedProbeTelemetryServiceServer
}

func (p *probeTelemetryServiceServer) SubmitProbeResult(ctx context.Context, req *def.SubmitProbeResultReq) (*def.SubmitProbeResultResp, error) {
	time := time2.Now().Unix()
	for _, v := range req.Summary {
		v.ServerReportReceivingTime = time
	}
	for _, collector := range probeResultCollectors {
		err := collector.SubmitProbeResult(ctx, req)
		if err != nil {
			return nil, err
		}
	}
	return &def.SubmitProbeResultResp{}, nil
}
func (p *probeTelemetryServiceServer) GetProbeConfig(context.Context, *def.GetConfigFileReq) (*def.GetConfigFileResp, error) {
	bridgelines, err := os.ReadFile("bridge_lines.txt")
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return &def.GetConfigFileResp{Configs: []*def.ConfigFileInfo{
		{Name: "bridge_lines.txt", Content: string(bridgelines)},
	}}, nil

}

func NewProbeTelemetryService() def.ProbeTelemetryServiceServer {
	return &probeTelemetryServiceServer{}
}
