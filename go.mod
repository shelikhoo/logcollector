module github.com/xiaokangwang/torprobeutil

go 1.17

require (
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.44.0
)

require (
	github.com/benhoyt/goawk v1.15.0 // indirect
	github.com/golang/protobuf v1.5.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
