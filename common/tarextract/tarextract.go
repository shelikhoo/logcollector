package tarextract

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"errors"
	"github.com/xiaokangwang/torprobeutil/common/testResultCollector"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

func NewTarExtractor(collector testResultCollector.Collector) testResultCollector.Extractor {
	return &tarExtractor{collector: collector}
}

type tarExtractor struct {
	collector testResultCollector.Collector
}

func (e *tarExtractor) Extract(path string) error {
	tarGzFile, err := os.Open(path)
	if err != nil {
		return err
	}
	gzFile, err := gzip.NewReader(tarGzFile)
	if err != nil {
		return err
	}
	tarStructure := tar.NewReader(gzFile)
	for {
		header, err := tarStructure.Next()
		if err != nil {
			if errors.Is(err, io.EOF) {
				return nil
			}
			return err
		}
		name := header.Name
		filename := filepath.Base(name)

		if strings.HasSuffix(filename, ".log") && !strings.HasSuffix(filename, "client.log") {
			machine := strings.TrimSuffix(filename, ".log")
			scanner := bufio.NewScanner(tarStructure)
			percentage := ""
			for scanner.Scan() {
				content := scanner.Text()
				exp := regexp.MustCompile("Bootstrapped ([0-9]+)%")
				if matched := exp.FindStringSubmatch(content); matched != nil {
					percentage = matched[1]
				}
			}
			out, _ := strconv.ParseInt(percentage, 10, 32)
			e.collector.OnCollectedBootstrapPercentage(machine, int(out))
		}
	}

}
