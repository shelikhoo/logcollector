package testResultCollector

import (
	"github.com/xiaokangwang/torprobeutil/common/def"
	time2 "time"
)

type Extractor interface {
	Extract(filepath string) error
}

type Collector interface {
	OnCollectedBridgeLine(name, bridgeLine string)
	OnCollectedBootstrapPercentage(name string, percentage int)
	GetData() map[string]*def.Summary
}

func NewLogCollector() Collector {
	return &collectorImpl{collected: map[string]*def.Summary{}}
}

type collectorImpl struct {
	collected map[string]*def.Summary
}

func (c *collectorImpl) ensureItemExist(name string) {
	if _, ok := c.collected[name]; !ok {
		c.collected[name] = &def.Summary{TestName: name, Detail: &def.TestDetail{}, ClientReportGenerationTime: time2.Now().Unix()}
	}
}

func (c *collectorImpl) OnCollectedBridgeLine(name, bridgeLine string) {
	c.ensureItemExist(name)
	c.collected[name].TestLine = bridgeLine
}

func (c *collectorImpl) OnCollectedBootstrapPercentage(name string, percentage int) {
	c.ensureItemExist(name)
	c.collected[name].Detail.BootstrapPercentage = uint64(percentage)
}

func (c *collectorImpl) GetData() map[string]*def.Summary {
	return c.collected
}
