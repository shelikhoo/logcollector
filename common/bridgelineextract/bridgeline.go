package bridgelineextract

import (
	"bufio"
	"bytes"
	"github.com/xiaokangwang/torprobeutil/common/testResultCollector"
	"io/ioutil"
	"strings"
)

func NewBridgeLineExtractor(collector testResultCollector.Collector) testResultCollector.Extractor {
	return &brLineExtractor{collector: collector}
}

type brLineExtractor struct {
	collector testResultCollector.Collector
}

func (e *brLineExtractor) Extract(filepath string) error {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}
	lineScanner := bufio.NewScanner(bytes.NewReader(data))
	for lineScanner.Scan() {
		lineContent := lineScanner.Text()
		lineElement := strings.SplitN(lineContent, ",", 2)
		e.collector.OnCollectedBridgeLine(lineElement[0], lineElement[1])
	}
	return nil
}
